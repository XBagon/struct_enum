extern crate proc_macro;

use quote::quote;
use syn::Visibility;
use syn::parse_macro_input;
use syn::parse2;
use syn::parse::{Parse, ParseStream, Result};
use syn::Token;
use syn::Ident;
use syn::ItemEnum;
use syn::ItemStruct;
use syn::braced;
use syn::spanned::Spanned;
use syn::punctuated::Punctuated;
use syn::Error;
use proc_macro::Span;

struct StructEnum {

}

impl Parse for StructEnum {
    fn parse(input: ParseStream) -> Result<Self> {
        let visibility: Visibility = input.parse()?;
        input.parse::<Token![enum]>()?;
        let name: Ident = input.parse()?;
        let block;
        braced!(block in input);
        let fields = Punctuated::<StructOrEnum, Token![,]>::parse_terminated(&block)?;
        Ok(StructEnum{})
    }
}

enum StructOrEnum {
    Struct(ItemStruct),
    Enum(ItemEnum),
}

impl Parse for StructOrEnum {
    fn parse(input: ParseStream) -> Result<Self> {
        if let Ok(strct) = input.parse::<ItemStruct>() {
            Ok(StructOrEnum::Struct(strct))
        } else if let Ok(enm) = input.parse::<ItemEnum>() {
            Ok(StructOrEnum::Enum(enm))
        } else {
            Err()
        }
    }
}

#[proc_macro]
pub fn struct_enum(input: proc_macro::TokenStream) -> proc_macro::TokenStream {
    quote!(

    ).into()
}