#[test]
fn it_works() {
    assert_eq!(2 + 2, 4);
}

extern crate struct_enum;
use struct_enum::struct_enum;

struct_enum!(
    pub enum Expression {
        pub struct Addition {
            augend: Box<Expression>,
            addend: Box<Expression>,
        },
        pub struct Subtraction {
            minuend: Box<Expression>,
            subtrahend: Box<Expression>,
        },
        pub struct Multiplication {
            multiplier: Box<Expression>,
            multiplicand: Box<Expression>,
        },
        pub struct Division {
            dividend: Box<Expression>,
            divisor: Box<Expression>,
        },
        pub struct Exponentiation {
            base: Box<Expression>,
            exponent: Box<Expression>,
        },
        pub struct Root {
            radicand: Box<Expression>,
            degree: Box<Expression>,
        },
        pub enum Operand<'a> {
            pub struct Variable {
                name: String
            },
            pub struct Literal<'a> {
                value: &'a num::Num
            },
        }
    }
);